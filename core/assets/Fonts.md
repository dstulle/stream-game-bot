# Fonts used in this Project

## ChunkFive
- License: SIL Open Font License
- Source: https://www.fontsquirrel.com/fonts/chunkfive

## DJ Gross
- License: SDFonts Font License
- Source: https://www.fontsquirrel.com/fonts/dj-gross
