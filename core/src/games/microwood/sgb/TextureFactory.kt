package games.microwood.sgb

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture

class TextureFactory {

    private val pixMap = Pixmap(1, 1, Pixmap.Format.RGBA8888)

    fun getColoredPixel(color: Color): Texture {
        pixMap.setColor(color)
        pixMap.fillRectangle(0, 0, 1, 1)
        return Texture(pixMap)
    }

    fun dispose() {
        pixMap.dispose()
    }

}