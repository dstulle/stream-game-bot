package games.microwood.sgb

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.Timer.Task
import com.github.philippheuer.credentialmanager.domain.OAuth2Credential
import com.github.philippheuer.events4j.api.domain.IEvent
import com.github.philippheuer.events4j.api.service.IEventHandler
import com.github.twitch4j.TwitchClient
import com.github.twitch4j.TwitchClientBuilder
import com.github.twitch4j.chat.events.channel.ChannelMessageEvent
import com.github.twitch4j.common.enums.CommandPermission
import kotlin.random.Random

class Bot(private val config: Config) : IEventHandler{

    private var roundStarted: Boolean = false

    private var twitchClient: TwitchClient

    private var state = State.WAITING_FOR_PLAYERS

    private enum class State {
        WAITING_FOR_PLAYERS, COUNTDOWN, STARTED, ROUND
    }

    val players = mutableSetOf<Player>()

    var playerOne: Player? = null
        private set
    var playerTwo: Player? = null
        private set

    var countdown: Int = 0
        private set

    var waitingForPlayersCountdown: Int = 0
        private set

    var winner: Player? = null
        private set

    class Player(val name: String = "") {
        val maxHealth = 5
        var health = maxHealth

        var matchesWon = 0

        val defeated: Boolean
            get() = health <= 0

        fun takeDamage(damage: Int = 1){
            this.health -= damage
        }

        override fun toString(): String {
            return this.name + " (" + this.health + "hp)" + "*".repeat(this.matchesWon)
        }

        override fun hashCode(): Int {
            return this.name.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            return when {
                this === other -> true
                other is Player -> this.name == other.name
                else -> false
            }

        }
    }

    init {
        val  oAuth2Credential = OAuth2Credential("twitch", config.twitchAccessToken)

        this.twitchClient = TwitchClientBuilder.builder()
                .withEnableChat(true)
                .withChatAccount(oAuth2Credential)
                .build()

        this.twitchClient.eventManager.eventHandlers.add(this)

        this.twitchClient.chat.joinChannel(config.channelName)

        this.players.add(Player("Player 1"))
        this.players.add(Player("Testplayer 1"))
        this.players.add(Player("other dude"))
        this.players.add(Player("Player X"))
        this.players.add(Player("Testplayer 2"))
        this.players.add(Player("other dudeine"))

        this.start()

    }

    fun update(delta: Float){
        when(this.state) {
            State.STARTED -> {
                if(this.playerOne == null && this.playerTwo == null) {
                    if(this.choosePlayerPair()){
                        Timer.schedule(object : Task(){
                            override fun run() {
                                state = State.ROUND
                            }
                        }, 3f)
                    } else if(this.players.any()) {
                        if(this.players.size == 1) { this.winner = this.players.first() }
                        Gdx.app.log(LOG_TAG, "The winner is: " + this.winner)
                        this.players.clear()
                        Timer.schedule(object : Task(){
                            override fun run() {
                                winner = null
                                state = State.WAITING_FOR_PLAYERS
                                waitingForPlayersCountdown = 0
                                this.cancel()
                            }
                        }, 10f)
                    }
                }
            }
            State.ROUND -> {
                if(!roundStarted) {
                    this.roundStarted = true
                    Timer.schedule(object : Task(){
                        override fun run() {
                            if(Random.nextBoolean()) {
                                playerTwo!!.takeDamage()
                                if(playerTwo!!.defeated){
                                    players.remove(playerTwo!!)
                                    playerOne!!.matchesWon += 1
                                    winner = playerOne
                                }
                            } else {
                                playerOne!!.takeDamage()
                                if(playerOne!!.defeated){
                                    players.remove(playerOne!!)
                                    playerTwo!!.matchesWon += 1
                                    winner = playerTwo
                                }
                            }
                            if(playerOne!!.defeated || playerTwo!!.defeated){
                                if(players.size >= 2) {
                                    Timer.schedule(object : Task(){
                                        override fun run() {
                                            if(playerOne?.defeated == false) {
                                                playerOne!!.health = playerOne!!.maxHealth
                                            }
                                            if(playerTwo?.defeated == false) {
                                                playerTwo!!.health = playerTwo!!.maxHealth
                                            }
                                            playerOne = null
                                            playerTwo = null
                                            winner = null
                                        }
                                    }, 2f)
                                } else {
                                    playerOne = null
                                    playerTwo = null
                                }
                                state = State.STARTED
                                roundStarted = false
                                this.cancel()
                            }
                        }
                    }, 0f, .75f)
                }
            }
            else -> {}
        }
    }

    fun dispose() {
        this.twitchClient.chat.leaveChannel(config.channelName)
    }


    override fun publish(event: IEvent) {
        Gdx.app.log(LOG_TAG, event.toString())
        if(event is ChannelMessageEvent && event.message.startsWith("!") && event.channel.name == config.channelName) {
            if(event.permissions.contains(CommandPermission.BROADCASTER)){
                if (event.message.removePrefix("!").startsWith("start")) {
                    this.start()
                }
            }
            if(this.state == State.WAITING_FOR_PLAYERS) {
                if (event.message.removePrefix("!").startsWith("fight")) {
                    addPlayerToNextFight(Player(event.messageEvent.tags["display-name"]!!))
                }
                //TODO: add !surrender command
            }
            println(event.messageEvent.tags["display-name"] + ": " + event.message)
        }
    }

    private fun start(){
        if(this.players.any() && this.state == State.WAITING_FOR_PLAYERS) {
            this.state = State.COUNTDOWN
            countdown = 3
            Timer.schedule(object : Task() {
                override fun run() {
                    countdown -= 1
                    println("set countdown to $countdown")
                    if(countdown == 0) {
                        state = State.STARTED
                        this.cancel()
                    }
                }
            }, 1f, 1f, 2)
        }
    }

    private fun addPlayerToNextFight(playerName: Player) {
        this.players.add(playerName)
        if (this.waitingForPlayersCountdown == 0) {
            this.waitingForPlayersCountdown = PLAYER_WAIT_TIME
            Timer.schedule(object : Task() {
                override fun run() {
                    waitingForPlayersCountdown -= 1
                    println("set waiting for players countdown to $waitingForPlayersCountdown")
                    if (waitingForPlayersCountdown == 0) {
                        start()
                        this.cancel()
                    }
                }
            }, 1f, 1f, PLAYER_WAIT_TIME-1)

        }
        Gdx.app.log(LOG_TAG, "current Players: " + this.players.toString())
    }

    private fun choosePlayerPair(): Boolean{
        this.playerOne = null
        this.playerTwo = null
        return when(this.players.size) {
            0 -> false
            1 -> false
            else -> {
                this.playerOne = this.players.random()
                val remainingPlayers = HashSet<Player>(this.players)
                remainingPlayers.remove(this.playerOne!!)
                this.playerTwo = remainingPlayers.random()
                true
            }
        }
    }

    override fun close() {
    }

    companion object {
        private val LOG_TAG = Bot::class.java.simpleName

        const val PLAYER_WAIT_TIME = 60
    }

}