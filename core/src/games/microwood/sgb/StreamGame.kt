package games.microwood.sgb

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.Json
import kotlin.system.exitProcess

class StreamGame : Game() {

    private lateinit var mainScreen: MainScreen
    private lateinit var bot: Bot

    override fun create() {
        val config = this.loadConfig()
        if(config != null) {
            this.bot = Bot(config)
            this.mainScreen = MainScreen(bot)
            this.setScreen(this.mainScreen)
        } else {
            Gdx.app.exit()
            exitProcess(-1)
        }
    }

    override fun dispose() {
        this.bot.dispose()
        this.mainScreen.dispose()
    }

    private fun loadConfig(configFileHandle: FileHandle = Gdx.files.absolute(this.getConfigFilePath())): Config? {
        return if(configFileHandle.exists()) {
            Gdx.app.log(LOG_TAG, "Loading configuration from: " + configFileHandle.path())
            val gameStateJson = configFileHandle.readString()
            fromJson(gameStateJson)
        } else {
            Gdx.app.error(LOG_TAG, "No configuration file was found. Please create a file at: " + configFileHandle.path())
            Gdx.app.error(LOG_TAG, "Content of the config file should be something like this:")
            Gdx.app.error(LOG_TAG, "{")
            Gdx.app.error(LOG_TAG, "    channelName: \"NAME_OF_YOUR_CHANNEL\",")
            Gdx.app.error(LOG_TAG, "    twitchAccessToken: \"oauth:YOUR_OAUTH_TOKEN\"")
            Gdx.app.error(LOG_TAG, "}")
            null
        }
    }

    private fun fromJson(gameStateJson: String): Config? {

        val json = Json()
        json.ignoreUnknownFields = true

        try {
            return json.fromJson(Config::class.java, gameStateJson)
        } catch (e: com.badlogic.gdx.utils.SerializationException) {
            Gdx.app.log(LOG_TAG, "Error while parsing configuration")
            Gdx.app.log(LOG_TAG, e.toString())
        }
        return null
    }

    private fun getConfigFilePath(): String {
        val path = with(System.getProperty("os.name").toLowerCase()){
            when {
                contains("win") -> System.getenv("APPDATA")
                contains("mac") -> System.getProperty("user.home") + "/Library"
                (contains("nix") || contains("nux")) -> System.getProperty("user.home")
                else -> System.getProperty("user.dir")
            }
        }
        return "$path/.sgb/config.json"
    }

    companion object {
        private val LOG_TAG = StreamGame::class.java.simpleName
    }
}