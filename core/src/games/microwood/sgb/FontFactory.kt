package games.microwood.sgb

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

class FontFactory {

    private val freeTypeFontParameter: FreeTypeFontGenerator.FreeTypeFontParameter =
        FreeTypeFontGenerator.FreeTypeFontParameter()

    private val blockFontGenerator = FreeTypeFontGenerator(
        Gdx.files.internal("ChunkFive-Regular.otf")
    )

    private val strokeFontGenerator = FreeTypeFontGenerator(
        Gdx.files.internal("DJGROSS.ttf")
    )

    init {
        freeTypeFontParameter.color = Color.WHITE
        freeTypeFontParameter.borderWidth = 2f
        freeTypeFontParameter.borderColor = Color.BLACK
    }

    fun getBlockFont(size: Int): BitmapFont {
        freeTypeFontParameter.size =size
        blockFontGenerator.generateData(freeTypeFontParameter)
        return blockFontGenerator.generateFont(freeTypeFontParameter)
    }

    fun geetStrokeFont(size: Int): BitmapFont {
        freeTypeFontParameter.size = size
        strokeFontGenerator.generateData(freeTypeFontParameter)
        return strokeFontGenerator.generateFont(freeTypeFontParameter)
    }

}