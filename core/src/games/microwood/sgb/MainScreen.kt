package games.microwood.sgb

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.IntMap

class MainScreen(private val bot: Bot) : ScreenAdapter() {
    private val spriteBatch = SpriteBatch()
    private val images = IntMap<Texture>()

    private val fontFactory = FontFactory()
    private val blockFont = fontFactory.getBlockFont(50)
    private val blockFontLarge = fontFactory.getBlockFont(100)
    private val blockFontXLarge = fontFactory.getBlockFont(300)
    private val strokeFontLarge = fontFactory.geetStrokeFont(100)
    private val strokeFontXLarge = fontFactory.geetStrokeFont(250)

    private val textureFactory = TextureFactory()
    private val redTexture = textureFactory.getColoredPixel(Color.RED)
    private val yellowTexture = textureFactory.getColoredPixel(Color.YELLOW)
    private val blackTexture = textureFactory.getColoredPixel(Color.BLACK)
    private val whiteTexture = textureFactory.getColoredPixel(Color.WHITE)

    private val testScreen = false

    init {
        initializeImages()
    }

    private fun initializeImages() {
        images.put(ROCK_ID, Texture("image-00.png"))
        images.put(PAPER_ID, Texture("image-01.png"))
        images.put(SCISSORS_ID, Texture("image-02.png"))
    }

    override fun render(delta: Float) {
        this.bot.update(delta)
        //TODO: make this background color configurable
        Gdx.gl.glClearColor(0f, 1f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.begin()
        when {
            this.testScreen -> {
                this.drawCenteredText(this.blockFontLarge, "blockFont", offsetY = -100f)
                this.blockFont.color = Color.RED
                this.drawCenteredText(this.blockFont, "red", offsetX = -250f)
                this.blockFont.color = Color.GREEN
                this.drawCenteredText(this.blockFont, "green", offsetX = 0f)
                this.blockFont.color = Color.BLUE
                this.drawCenteredText(this.blockFont, "blue", offsetX = 250f)
                this.blockFont.color = Color.WHITE
                this.drawCenteredText(this.strokeFontLarge, "strokeFont", offsetY = 100f)
            }
            this.bot.waitingForPlayersCountdown != 0 -> {
                val displayText = "Next match starting in " + this.bot.waitingForPlayersCountdown + " seconds. Type '!fight' in chat to join next round.\n\nplayers:\n " + bot.players.joinToString(", ")
                this.drawCenteredText(this.blockFont, displayText)
            }
            this.bot.countdown != 0 -> {
                this.drawCenteredText(this.blockFontXLarge, this.bot.countdown.toString())
            }
            this.bot.playerOne != null && this.bot.playerTwo != null -> {
                if(this.bot.winner == null) {
                    this.strokeFontXLarge.color = Color.RED
                    this.drawCenteredText(this.strokeFontXLarge, "vs", offsetY = +100f)
                    this.drawCenteredText(this.blockFontLarge, this.bot.playerOne!!.name, offsetX = - Gdx.graphics.width/4f, offsetY = +100f, targetWidth = Gdx.graphics.width.toFloat()/3)
                    this.drawCenteredText(this.blockFontLarge, this.bot.playerTwo!!.name, offsetX = + Gdx.graphics.width/4f, offsetY = +100f, targetWidth = Gdx.graphics.width.toFloat()/3)
                } else {
                    this.drawCenteredText(this.blockFontLarge, this.bot.winner!!.name + " wins", offsetY = 100f)
                }
                this.drawHealthBar(this.bot.playerOne!!, 100f, Gdx.graphics.height-100f, Gdx.graphics.width/2-200f, 50f, false)
                this.drawHealthBar(this.bot.playerTwo!!, Gdx.graphics.width/2+100f, Gdx.graphics.height-100f, Gdx.graphics.width/2-200f, 50f, true)
                this.drawCenteredText(this.blockFont, "remaining players:\n " + bot.players.joinToString(", "), offsetY = -400f, targetWidth = Gdx.graphics.width.toFloat())
            }
            this.bot.playerOne == null && this.bot.playerTwo == null && this.bot.winner != null -> {
                this.drawCenteredText(this.blockFontLarge, "a winner is you:\n" + bot.winner, targetWidth = Gdx.graphics.width.toFloat())
            }
            this.bot.players.any() -> {
                this.drawCenteredText(this.blockFont, "players:\n " + bot.players.joinToString(", "))
            }
        }
        spriteBatch.end()
    }

    private fun drawHealthBar(player: Bot.Player, x: Float, y: Float, width: Float, height: Float, leftToRight: Boolean) {
        this.spriteBatch.draw(this.blackTexture, x-2, y-2, width+4, height+4)
        this.spriteBatch.draw(this.whiteTexture, x, y, width, height)
        this.spriteBatch.draw(this.redTexture, x+2, y+2, width-4, height-4)
        if(player.health >= 0) {
            if(leftToRight) {
                this.spriteBatch.draw(this.yellowTexture, x+2, y+2, (width-4) * player.health/player.maxHealth, height-4)
            } else {
                this.spriteBatch.draw(this.yellowTexture, x+2 + (width-4) * (1-player.health.toFloat()/player.maxHealth.toFloat()), y+2, (width-4) * player.health/player.maxHealth, height-4)
            }
        }
    }

    private fun drawCenteredText(bitmapFont: BitmapFont, displayText: String, offsetX: Float = 0f, offsetY: Float = 0f, targetWidth: Float = Gdx.graphics.width/2f, align: Int = Align.center) {
        val glyphLayout = GlyphLayout(
                bitmapFont,
                displayText,
                Color.BLACK,
                targetWidth,
                align,
                true
        )
        bitmapFont.draw(
                this.spriteBatch,
                displayText,
                Gdx.graphics.width/2f-targetWidth/2f + offsetX,
                Gdx.graphics.height/2f + glyphLayout.height/2f + offsetY,
                targetWidth,
                align,
                true
        )
    }

    override fun dispose() {
        super.dispose()
        spriteBatch.dispose()
        textureFactory.dispose()
        for (image in images.values()){
            image.dispose()
        }
    }

    companion object {
        const val ROCK_ID = 0
        const val PAPER_ID = 1
        const val SCISSORS_ID = 2
    }

}