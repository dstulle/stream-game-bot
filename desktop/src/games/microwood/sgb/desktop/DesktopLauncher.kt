package games.microwood.sgb.desktop

import kotlin.jvm.JvmStatic
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import games.microwood.sgb.StreamGame

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        //TODO: read size from config file or parameter
        config.width = 1920
        config.height = 1080
        config.title = "SGB - stream game bot"
//        config.forceExit = false
        LwjglApplication(StreamGame(), config)
    }
}